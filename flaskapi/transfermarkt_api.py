from flask import Flask
from flask_restx import Api, Resource
import transfermarkt


app = Flask(__name__)
app.config.SWAGGER_UI_DOC_EXPANSION = 'list'
api = Api(app, version='1.0', title='Unofficial Transfermarkt API', description='An unofficial Transfermarkt API')


@api.route("/search/<search_term>", endpoint='search', methods=["GET"])
@api.doc(params={'search_term': 'A string of the search term'})
class Search(Resource):
    @staticmethod
    def get(search_term):
        """
        Get search results for a given search term.

        :param search_term: str
        :return: json
        """
        output = transfermarkt.get_search(search_term)
        return {"message": output['message'], "data": output['data']}, output['status_code']


@api.route("/continents", endpoint='continents', methods=["GET"])
class Continents(Resource):
    @staticmethod
    def get():
        """
        Get available continents.

        :return: json
        """
        output = transfermarkt.get_continents()
        return {"message": output['message'], "data": output['data']}, output['status_code']


@api.route('/countries/<continent>', endpoint='countries', methods=["GET"])
@api.doc(params={'continent': 'A string of the continent'})
class Countries(Resource):
    @staticmethod
    def get(continent):
        """
        Get available countries for a continent.

        :param continent: str
        :return: json
        """
        output = transfermarkt.get_countries(continent)
        return {"message": output['message'], "data": output['data']}, output['status_code']


@api.route("/competitions/<int:country_id>/<int:season>", endpoint='competitions', methods=["GET"])
@api.doc(params={'country_id': 'An interger of the country id',
                 'season': 'An integer of the start year for a season'})
class Competitions(Resource):
    @staticmethod
    def get(country_id, season):
        """
        Get available competitions for a country id ands season.

        To get Germany competitions for season 2020/21
        /competitions/40/2020

        :param country_id: int
        :param season: int
        :return: json
        """
        output = transfermarkt.get_competitions(country_id, season)
        return {"message": output['message'], "data": output['data']}, output['status_code']


@api.route("/teams/<competition_id>/<int:season>", endpoint='teams', methods=["GET"])
@api.doc(params={'competition_id': 'A string of the competition id',
                 'season': 'An integer of the start year for a season'})
class Teams(Resource):
    @staticmethod
    def get(competition_id, season):
        """
        Get available teams for a competition id and season.

        To get German 2.Bundesliga teams for season 2020/21
        /teams/L2/2020

        :param competition_id: str
        :param season: int
        :return: json
        """
        output = transfermarkt.get_teams(competition_id, season)
        return {"message": output['message'], "data": output['data']}, output['status_code']


@api.route("/squad/<int:team_id>/<int:season>", endpoint='squad', methods=["GET"])
@api.doc(params={'team_id': 'An integer of the team id',
                 'season': 'An integer of the start year for a season'})
class Squad(Resource):
    @staticmethod
    def get(team_id, season):
        """
        Get available squad for a team id and season.

        To get playing squad for German team Hansa Rostock for season 2020/21
        /squad/30/2020

        :param team_id: int
        :param season: int
        :return: json
        """
        output = transfermarkt.get_squad(team_id, season)
        return {"message": output['message'], "data": output['data']}, output['status_code']


@api.route("/squad_stats/<int:team_id>/<int:season>", endpoint='squad_stats', methods=["GET"])
@api.doc(params={'team_id': 'A integer of the team id',
                 'season': 'An integer of the start year for a season'})
class SquadStats(Resource):
    @staticmethod
    def get(team_id, season):
        """
        Get available squad stats for a team id and season.

        To get playing squad statistics for English team Reading for season 2020/21
        /squad_stats/1032/2020

        :param team_id: int
        :param season: int
        :return: json
        """
        output = transfermarkt.get_squad_stats(team_id, season)
        return {"message": output['message'], "data": output['data']}, output['status_code']


if __name__ == '__main__':
    app.run()