def name(column):
    """
    Get team name

    :param column: bs4 object
    :return: str
    """
    return column.a.text


def slug(column):
    """
    Get team slug name

    :param column: bs4 object
    :return: str
    """
    return column.a['href'].split("/")[1]


def unique_identifier(column):
    """
    Get team id

    :param column: bs4 object
    :return: str
    """
    return column.a['id']
