import unittest
from unittest.mock import patch
import api


class TestAPI(unittest.TestCase):

    def setUp(self):
        self.api = api.API()

    def test_search(self):

        with patch("api.API.search") as mock_get:
            mock_get.return_value.status_code = 200

            response = self.api.search("Bowyer")

        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
