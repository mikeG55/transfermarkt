import unittest
from bs4 import BeautifulSoup
import team


class TestTeam(unittest.TestCase):

    def setUp(self):
        column = """<td class="hauptlink no-border-links show-for-small show-for-pad">
                        <a class="vereinprofil_tooltip" 
                        href="/wurzburger-kickers/startseite/verein/1557/saison_id/2020" id="1557">Würzb. Kickers</a>
                    </td>"""
        self.soup = BeautifulSoup(column, 'html.parser')

    def test_name(self):
        self.assertEqual(team.name(self.soup), "Würzb. Kickers")

    def test_slug(self):
        self.assertEqual(team.slug(self.soup), "wurzburger-kickers")

    def test_unique_identifier(self):
        self.assertEqual(team.unique_identifier(self.soup), "1557")


if __name__ == '__main__':
    unittest.main()
