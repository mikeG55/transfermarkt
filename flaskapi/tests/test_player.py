import unittest
from bs4 import BeautifulSoup
import player


class TestPlayer(unittest.TestCase):

    def test_unique_identifier(self):
        column = """<td>
                        <table class="inline-table">
                        <tr>
                        <td rowspan="2">
                        <a href="#">
                        <img alt="George Bowyer" class="bilderrahmen-fixed" 
                        src="https://img.a.transfermarkt.technology/portrait/small/default.jpg?lm=1" 
                        title="George Bowyer"/></a></td><td class="hauptlink">
                        <a class="spielprofil_tooltip" href="/george-bowyer/profil/spieler/77327" id="77327" 
                        title="George Bowyer">George Bowyer</a>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <a class="vereinprofil_tooltip" href="/fc-droylsden/startseite/verein/14772" 
                        id="14772">FC Droylsden
                        </a>
                        </td>
                        </tr>
                        </table>
                        </td>
                        """
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.unique_identifier(soup), "77327")

    def test_name(self):
        column = """<td class="posrela" title=""><table class="inline-table" title=""><tr><td class="" rowspan="2">
        <a href="#"><img alt="Eddie Nketiah" class="bilderrahmen-fixed" 
        src="https://img.a.transfermarkt.technology/portrait/small/340324-1508912874.jpg?lm=1" title="Eddie Nketiah"/>
        </a></td><td class="hauptlink"><div class="di nowrap"><span class="hide-for-small">
        <a class="spielprofil_tooltip" href="/eddie-nketiah/profil/spieler/340324" id="340324" title="Eddie Nketiah">
        Eddie Nketiah</a></span></div><div class="di nowrap"><span class="show-for-small"><a class="spielprofil_tooltip"
         href="/eddie-nketiah/profil/spieler/340324" id="340324" title="Eddie Nketiah">E. Nketiah</a></span></div></td>
         </tr><tr><td>Centre-Forward</td></tr></table></td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.name(soup), "Eddie Nketiah")

    def test_dob(self):
        column = """<td class="zentriert">May 30, 1999 (21)</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.dob(soup), ("1999-05-30", "21"))

        column = """<td class="zentriert">- (-)</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.dob(soup), (None, None))

        column = """<td class="zentriert"><a href="/intern/faq/#ds_info">N/A</a> (27)</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.dob(soup), (None, '27'))

    def test_nationality(self):
        column = """<td class="zentriert"><img alt="Germany" class="flaggenrahmen" 
                src="https://tmssl.akamaized.net/images/flagge/verysmall/40.png?lm=1520612525" title="Germany"/></td>
                """
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.nationality(soup), "Germany")

    def test_height(self):
        column = """<td class="zentriert">1,85 m</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.height(soup), "1.85 m")

        column = """<td class="zentriert"> m</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.height(soup), " m")

        column = """<td class="zentriert"><a href="/intern/faq/#ds_info">N/A</a></td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.height(soup), "N/A")

    def test_foot(self):
        column = """<td class="zentriert">right</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.foot(soup), "right")

        column = """<td class="zentriert">left</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.foot(soup), "left")

        column = """<td class="zentriert">both</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.foot(soup), "both")

        column = """<td class="zentriert">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.foot(soup), None)

        column = """<td class="zentriert"><a href="/intern/faq/#ds_info">N/A</a></td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.foot(soup), "N/A")

    def test_joined(self):
        column = """<td class="zentriert">Jul 1, 2006</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.joined(soup), "2006-07-01")

        column = """<td class="zentriert">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.joined(soup), None)

    def test_signed_from(self):
        column = """<td class="zentriert"><a class="vereinprofil_tooltip" 
                href="/germania-halberstadt/startseite/verein/364/saison_id/2018" id="364">
                <img alt="Germania Halberstadt" class="" 
                src="https://tmssl.akamaized.net/images/wappen/verysmall/364.png?lm=1497129592" 
                title=": Ablöse free transfer"/></a></td>
                """
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.signed_from(soup), "Germania Halberstadt")

        column = """<td class="zentriert"></td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.signed_from(soup), None)

    def test_market_value(self):
        column = """<td class="rechts hauptlink">€125Th. <span class="icons_sprite green-arrow-ten" 
        title="Vorheriger Marktwert: €100Th."> </span></td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.market_value(soup), '€125Th')

        column = """<td class="rechts hauptlink">- </td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.market_value(soup), None)

    def test_contract_expires(self):
        column = """<td class="zentriert">Jun 30, 2023</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.contract_expires(soup), "2023-06-30")

        column = """<td class="zentriert">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.contract_expires(soup), None)

    def test_in_squad(self):
        column = """<td class="zentriert">39</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.in_squad(soup), "39")

        column = """<td class="zentriert">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.in_squad(soup), 0)

    def test_appearances(self):
        column = """<td class="zentriert" colspan="1">33</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.appearances(soup), '33')

        column = """<td class="zentriert" colspan="10">Not in squad during this season</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.appearances(soup), 'Not in squad during this season')

        column = """<td class="zentriert" colspan="10">Not used during this season</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.appearances(soup), 'Not used during this season')

    def test_goals(self):
        column = """<td class="zentriert">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.goals(soup), '0')

        column = """<td class="zentriert hide">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.goals(soup), '0')

        column = """<td class="zentriert">7</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.goals(soup), '7')

    def test_assists(self):
        column = """<td class="zentriert">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.assists(soup), '0')

        column = """<td class="zentriert hide">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.assists(soup), '0')

        column = """<td class="zentriert">3</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.assists(soup), '3')

    def test_yellow_cards(self):
        column = """<td class="zentriert">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.yellow_cards(soup), '0')

        column = """<td class="zentriert hide">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.yellow_cards(soup), '0')

        column = """<td class="zentriert">3</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.yellow_cards(soup), '3')

    def test_second_yellow_cards(self):
        column = """<td class="zentriert">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.second_yellow_cards(soup), '0')

        column = """<td class="zentriert hide">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.second_yellow_cards(soup), '0')

        column = """<td class="zentriert">2</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.second_yellow_cards(soup), '2')

    def test_red_cards(self):
        column = """<td class="zentriert">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.red_cards(soup), '0')

        column = """<td class="zentriert hide">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.red_cards(soup), '0')

        column = """<td class="zentriert">1</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.red_cards(soup), '1')

    def test_subbed_on(self):
        column = """<td class="zentriert">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.subbed_on(soup), '0')

        column = """<td class="zentriert hide">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.subbed_on(soup), '0')

        column = """<td class="zentriert">10</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.subbed_on(soup), '10')

    def test_subbed_off(self):
        column = """<td class="zentriert">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.subbed_off(soup), '0')

        column = """<td class="zentriert hide">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.subbed_off(soup), '0')

        column = """<td class="zentriert">8</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.subbed_off(soup), '8')

    def test_ppg(self):
        column = """<td class="zentriert cp greentext" title="Points average of club: 2.13">2.19</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.ppg(soup), '2.19')

        column = """<td class="zentriert cp" title="Points average of club: 2.13">2.13</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.ppg(soup), '2.13')

        column = """<td class="zentriert cp hide" title="Points average of club: 2.13">0</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.ppg(soup), '0')

        column = """<td class="zentriert cp redtext" title="Points average of club: 2.13">2.00</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.ppg(soup), '2.00')

    def test_minutes_played(self):
        column = """<td class="rechts">2.970'</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.minutes_played(soup), '2970')

        column = """<td class="rechts hide">-</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.minutes_played(soup), '0')

        column = """<td class="rechts">185'</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(player.minutes_played(soup), '185')


if __name__ == '__main__':
    unittest.main()
