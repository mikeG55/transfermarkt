import unittest
from bs4 import BeautifulSoup
import agent


class TestAgent(unittest.TestCase):

    def test_name(self):
        column = """<td>Dan Bowyer</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(agent.name(soup), "Dan Bowyer")

    def test_company(self):
        column = """<td class="zentriert">
                        <table class="inline-table" ltd="" title="Omni-Sports">
                            <tr>
                                <td rowspan="2">
                                    <a href="/berater/beraterfirma?id=4434"><img alt="-" class="berater-logorahmen" 
                                    src="https://tmssl.akamaized.net/images/berater_logo/small/berater-default.png?lm=1520606995" 
                                    title="Omni-Sports Ltd"/>
                                    </a>
                                </td>
                                <td class="hauptlink">
                                    <a href="/omni-sports-ltd/beraterfirma/berater/4434">Omni-Sports Ltd
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(agent.company(soup), "Omni-Sports Ltd")


if __name__ == '__main__':
    unittest.main()
