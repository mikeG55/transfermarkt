import unittest
from bs4 import BeautifulSoup
import competition


class TestCompetition(unittest.TestCase):

    def setUp(self):
        column = """
                <td class="hauptlink">
                <table class="inline-table">
                <tr>
                    <td>
                        <a href="/landespokal-niedersachsen-amateure-/startseite/wettbewerb/Landespokal+Niedersachsen+
                        %28Amateure%29">
                        <img alt="Landespokal Niedersachsen (Amateure)" class="" 
                        src="https://tmssl.akamaized.net/images/logo/tiny/npam.png?lm=1538742317" 
                        title="Landespokal Niedersachsen (Amateure)"/> </a>
                    </td>
                    <td>
                    <a href="/landespokal-niedersachsen-amateure-/startseite/wettbewerb/NPAM" 
                    title="Landespokal Niedersachsen (Amateure)">Niedersachsenpokal (Amateure)</a>
                    </td>
                </tr>
                </table>
                </td>"""
        self.soup = BeautifulSoup(column, 'html.parser')

    def test_name(self):
        self.assertEqual(competition.name(self.soup), "Niedersachsenpokal (Amateure)")

    def test_slug(self):
        self.assertEqual(competition.slug(self.soup), "landespokal-niedersachsen-amateure-")

    def test_unique_identifier(self):
        self.assertEqual(competition.unique_identifier(self.soup), "NPAM")


if __name__ == '__main__':
    unittest.main()
