import unittest
from unittest.mock import patch
import transfermarkt_api


class TestTransfermarktAPI(unittest.TestCase):

    def test_get_search(self):
        fake_response = {"message": "OK", "data":
            {'Search results for players':  [{'id': '77327', 'name': 'George Bowyer'},
                                             {'id': '213386', 'name': 'Michael Nonni'},
                                             {'id': '3877', 'name': 'Lee Bowyer'}],
             'Search results: Managers & officials': [{'id': '0', 'name': 'Ian Bowyer', 'position': 'Scout'},
                                                      {'id': '0', 'name': 'Gary Bowyer', 'position': 'Manager'},
                                                      {'id': '0', 'name': 'Lee Bowyer', 'position': 'Manager'}],
             'Search results for agents': [{'name': 'Dan Bowyer', 'company': 'Omni-Sports Ltd'}]
             }
        }

        with patch("transfermarkt_api.Search.get") as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.Search.get()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), fake_response)

    def test_get_continents(self):
        fake_response = {"message": "OK", "data": ["europa", "asien"]}

        with patch("transfermarkt_api.Continents.get") as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.Continents.get()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), fake_response)

    def test_get_countries(self):
        fake_response = {"message": "OK", "data": [{"id": "189", "name": "England"}, {"id": "75", "name": "Italy"},
                                                   {"id": "157", "name": "Spain"}, {"id": "40", "name": "Germany"},
                                                   {"id": "50", "name": "France"}, {"id": "136", "name": "Portugal"}]}

        with patch("transfermarkt_api.Countries.get") as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.Countries.get("europa")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), fake_response)

        fake_response = {"message": "Not Found", "data": []}
        with patch("transfermarkt_api.Countries.get") as mock_get:
            mock_get.return_value.status_code = 404
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.Countries.get("oceania")

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), fake_response)

    def test_get_competitions(self):
        fake_response = {"message": "OK", "data": [{"id": "IT1", "name": "Serie A", "slug": "serie-a"},
                                                   {"id": "IT2", "name": "Serie B", "slug": "serie-b"},
                                                   {"id": "IT3A", "name": "Serie C - A", "slug": "serie-c-girone-a"},
                                                   {"id": "IT3B", "name": "Serie C - B", "slug": "serie-c-girone-b"}]}

        with patch("transfermarkt_api.Competitions.get") as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.Competitions.get(75, 2020)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), fake_response)

        fake_response = {"message": "Not Found", "data": []}
        with patch("transfermarkt_api.Competitions.get") as mock_get:
            mock_get.return_value.status_code = 404
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.Competitions.get(0, 0)

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), fake_response)

    def test_get_teams(self):
        fake_response = {"message": "OK", "data": [{"id": "989", "name": "Bournemouth", "slug": "afc-bournemouth"},
                                                   {"id": "1123", "name": "Norwich", "slug": "norwich-city"},
                                                   {"id": "1148", "name": "Brentford", "slug": "fc-brentford"},
                                                   {"id": "1010", "name": "Watford", "slug": "fc-watford"}]}

        with patch("transfermarkt_api.Teams.get") as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.Teams.get("GB2", 2020)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), fake_response)

        fake_response = {"message": "Not Found", "data": []}
        with patch("transfermarkt_api.Teams.get") as mock_get:
            mock_get.return_value.status_code = 404
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.Teams.get(0, 0)

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), fake_response)

    def test_get_squad(self):
        fake_response = {"message": "OK", "data": [{"name": "Lee Camp", "dob": "1984-08-22"}]}

        with patch("transfermarkt_api.Squad.get") as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.Squad.get(352, 2020)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), fake_response)

        # TODO should this be internal server error. Comes from TM.
        fake_response = {"message": "Internal Server Error", "data": []}
        with patch("transfermarkt_api.Squad.get") as mock_get:
            mock_get.return_value.status_code = 500
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.Squad.get(0, 0)

        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.json(), fake_response)

        fake_response = {"message": "Not Found", "data": []}
        with patch("transfermarkt_api.Squad.get") as mock_get:
            mock_get.return_value.status_code = 404
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.Squad.get(1, 0)

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), fake_response)

    def test_get_squad_stats(self):
        fake_response = {"message": "OK", "data": [{"name": "Mark Travers", "in_squad": "8", "appearances": "8",
                                                    "goals": 0, "assists": 0, "yellow_cards": 0,
                                                    "second_yellow_cards": 0, "red_cards": 0, "subbed_on": 0,
                                                    "subbed_off": 0, "ppg": "0.88", "minutes_played": "720"}]}

        with patch("transfermarkt_api.SquadStats.get") as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.SquadStats.get(352, 2020)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), fake_response)

        # TODO should this be internal server error. Comes from TM.
        fake_response = {"message": "Internal Server Error", "data": []}
        with patch("transfermarkt_api.SquadStats.get") as mock_get:
            mock_get.return_value.status_code = 500
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.SquadStats.get(0, 0)

        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.json(), fake_response)

        # TODO should this be internal server error. Comes from TM.
        fake_response = {"message": "Internal Server Error", "data": []}
        with patch("transfermarkt_api.SquadStats.get") as mock_get:
            mock_get.return_value.status_code = 500
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.SquadStats.get(0, 1)

        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.json(), fake_response)

        fake_response = {"message": "Not Found", "data": []}
        with patch("transfermarkt_api.SquadStats.get") as mock_get:
            mock_get.return_value.status_code = 404
            mock_get.return_value.json.return_value = fake_response

            response = transfermarkt_api.SquadStats.get(1, 0)

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), fake_response)


if __name__ == '__main__':
    unittest.main()
