import unittest
from bs4 import BeautifulSoup
import country


class TestCountry(unittest.TestCase):

    def setUp(self):
        column = """<td class="zentriert">
                        <img alt="Romania" class="flaggenrahmen" 
                            src="https://tmssl.akamaized.net/images/flagge/tiny/140.png?lm=1520611569" 
                            title="Romania"/>
                    </td>"""
        self.soup = BeautifulSoup(column, 'html.parser')

    def test_name(self):
        self.assertEqual(country.name(self.soup), "Romania")

    def test_unique_identifier(self):
        self.assertEqual(country.unique_identifier(self.soup), "140")


if __name__ == '__main__':
    unittest.main()
