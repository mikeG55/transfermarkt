import unittest
from bs4 import BeautifulSoup
import manager_official


class TestManagerOfficial(unittest.TestCase):

    def setUp(self):
        column = """<td>
                        <table class="inline-table">
                            <tr>
                                <td rowspan="2">
                                    <a href="#">
                                        <img alt="Ian Bowyer" class="bilderrahmen-fixed" 
                                        src="https://img.a.transfermarkt.technology/portrait/small/default.jpg?lm=1" 
                                        title="Ian Bowyer"/></a></td><td class="hauptlink">
                                    <a href="/ian-bowyer/profil/trainer/1715" id="0" title="Ian Bowyer">Ian Bowyer
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="vereinprofil_tooltip" 
                                    href="/fc-portsmouth/startseite/verein/1020" id="1020">Portsmouth FC
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>"""
        self.soup = BeautifulSoup(column, 'html.parser')

    def test_unique_identifier(self):
        self.assertEqual(manager_official.unique_identifier(self.soup), "0")

    def test_name(self):
        self.assertEqual(manager_official.name(self.soup), "Ian Bowyer")

    def test_position(self):
        column = """<td class="rechts">Scout</td>"""
        soup = BeautifulSoup(column, 'html.parser')
        self.assertEqual(manager_official.position(soup), "Scout")


if __name__ == '__main__':
    unittest.main()
