import unittest
import helpers


class TestHelpers(unittest.TestCase):

    def test_month_number(self):
        self.assertEqual(helpers.month_number("Jan"), "01")
        self.assertEqual(helpers.month_number("Feb"), "02")
        self.assertEqual(helpers.month_number("Mar"), "03")
        self.assertEqual(helpers.month_number("Apr"), "04")
        self.assertEqual(helpers.month_number("May"), "05")
        self.assertEqual(helpers.month_number("Jun"), "06")
        self.assertEqual(helpers.month_number("Jul"), "07")
        self.assertEqual(helpers.month_number("Aug"), "08")
        self.assertEqual(helpers.month_number("Sep"), "09")
        self.assertEqual(helpers.month_number("Oct"), "10")
        self.assertEqual(helpers.month_number("Nov"), "11")
        self.assertEqual(helpers.month_number("Dec"), "12")


if __name__ == '__main__':
    unittest.main()
