def name(column):
    """
    Get competition name

    :param column: bs4 object
    :return: str
    """
    # data is in the second a tag.
    return column.find_all('a')[-1].text


def slug(column):
    """
    Get competition name

    :param column: bs4 object
    :return: str
    """
    # data is in the second a tag.
    return column.find_all('a')[-1]['href'].split("/")[1]


def unique_identifier(column):
    """
    Get competition id

    :param column: bs4 object
    :return: str
    """
    # data is in the second a tag.
    return column.find_all('a')[-1]['href'].split("/")[-1]


