import requests


class API(object):
    """
    """

    def __init__(self):
        """

        """
        self.url = "https://www.transfermarkt.com/"
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36'}

    def search(self, search_term):
        """
        Search transfermarkt.

        :param search_term: str
        :return: requests obj
        """
        search_term = search_term.replace(" ", "+")
        url = f"{self.url}schnellsuche/ergebnis/schnellsuche?query={search_term}"
        response = requests.get(url, headers=self.headers)

        return response

    def get_countries(self, continent_slug):
        """

        :param continent_slug: str
        :return: requests obj
        """
        url = f"{self.url}wettbewerbe/{continent_slug}/wettbewerbe?plus=1"
        response = requests.get(url, headers=self.headers)

        return response

    def get_competitions(self, country_id, season):
        """

        :param country_id: int
        :param season: int
        :return: requests obj
        """
        url = f"{self.url}wettbewerbe/national/wettbewerbe/{country_id}/saison_id/{season}/plus/1"
        response = requests.get(url, headers=self.headers)

        return response

    def get_teams(self, competition_id, season):
        """

        :param competition_id: str
        :param season: int
        :return: requests obj
        """
        url = f"{self.url}competition/startseite/wettbewerb/{competition_id}/saison_id/{season}/plus/1"
        response = requests.get(url, headers=self.headers)

        return response

    def get_squad(self, team_id, season):
        """

        :param team_id int
        :param season: int
        :return:
        """
        url = f"{self.url}team/kader/verein/{team_id}/saison_id/{season}/plus/1"
        response = requests.get(url, headers=self.headers)

        return response

    def get_squad_stats(self, team_id, season):
        """

        :param team_id: int
        :param season: int
        :return: requests obj
        """
        url = f"{self.url}team/leistungsdaten/verein/{team_id}/reldata/%26{season}/plus/1"
        response = requests.get(url, headers=self.headers)

        return response

    def get_player_profile(self, player_id):
        """
        Gets a player profile for a given player_id {72476}.
        Returns a requests response object.

        :param player: str
        :param player_id: int
        :return: requests obj
        """
        url = f"{self.url}player/profil/spieler/{player_id}"
        response = requests.get(url, headers=self.headers)

        return response

    def player_market_value(self, player_id):
        """
        Get a players market history value for a given player id.

        :param player_id: int
        :return: requests obj
        """
        url = f"{self.url}player/marktwertverlauf/spieler/{player_id}"
        response = requests.get(url, headers=self.headers)

        return response
