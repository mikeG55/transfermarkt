"""
Global configuration objects for the project.
"""
import os
config = {
    "app": {
        "project_name": "transfermarkt",
        "root_path": os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    }
}