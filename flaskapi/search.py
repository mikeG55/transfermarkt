import player
import team
import manager_official
import agent


def players(search_table):
    """

    :param search_table: bs4 object
    :return: list
    """
    output = []
    header_row = search_table.thead.tr
    headers = header_row.find_all('th')
    headers = [h.text for h in headers]

    # player
    # ['Name/Position', 'Position', 'Club', 'Age', 'Nat.', 'Market Value', 'Agents']

    content_rows = search_table.tbody
    content_rows = content_rows.find_all('tr', recursive=False)
    for row in content_rows:
        details = {}
        columns = row.find_all('td', recursive=False)
        row_dict = dict(zip(headers, columns))
        details['id'] = player.unique_identifier(row_dict['Name/Position'])
        details['name'] = player.name(row_dict['Name/Position'])
        output.append(details)

    return output


def teams(search_table):
    """

    :param search_table: bs4 object
    :return: list
    """
    output = []
    header_row = search_table.thead.tr
    headers = header_row.find_all('th')
    headers = [h.text for h in headers]

    # teams
    # ['\xa0', 'Club', 'Country', 'Squad', 'Total Market Value', 'Transfers', 'Stadium', 'forum']

    content_rows = search_table.tbody
    content_rows = content_rows.find_all('tr', recursive=False)
    for row in content_rows:
        details = {}
        columns = row.find_all('td', recursive=False)
        row_dict = dict(zip(headers, columns))
        details['id'] = team.unique_identifier(row_dict['Club'])
        details['name'] = team.name(row_dict['Club'])
        output.append(details)

    return output


def managers_and_officials(search_table):
    """

    :param search_table: bs4 object
    :return: list
    """
    output = []
    header_row = search_table.thead.tr
    headers = header_row.find_all('th')
    headers = [h.text for h in headers]
    # ['Name', 'Club', 'Age', 'Nat.', 'Function', 'Contract until']

    content_rows = search_table.tbody
    content_rows = content_rows.find_all('tr', recursive=False)
    for row in content_rows:
        details = {}
        columns = row.find_all('td', recursive=False)
        row_dict = dict(zip(headers, columns))
        details['id'] = manager_official.unique_identifier(row_dict['Name'])
        details['name'] = manager_official.name(row_dict['Name'])
        details['position'] = manager_official.position(row_dict['Function'])
        output.append(details)

    return output


def agents(search_table):
    """

    :param search_table: bs4 object
    :return: list
    """
    output = []
    header_row = search_table.thead.tr
    headers = header_row.find_all('th')
    headers = [h.text for h in headers]
    # ['Premium Service', 'Company', 'Licence', 'Agents']

    content_rows = search_table.tbody
    content_rows = content_rows.find_all('tr', recursive=False)
    for row in content_rows:
        details = {}
        columns = row.find_all('td', recursive=False)
        row_dict = dict(zip(headers, columns))
        details['name'] = agent.name(row_dict['Agents'])
        details['company'] = agent.company(row_dict['Company'])
        output.append(details)

    return output
