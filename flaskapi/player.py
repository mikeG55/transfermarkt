from helpers import month_number


def unique_identifier(column):
    """

    :param column: bs4 object
    :return: str
    """
    return column.find("a", {"class": "spielprofil_tooltip"})['id']


def name(column):
    """

    :param column: bs4 object
    :return: str
    """
    return column.find("a", {"class": "spielprofil_tooltip"}).text.strip()


def dob(column):
    """
    Gets the date of birth and age from a string. Reformats date of bith to YYYY-MM-DD
    Returns data as dictionary.

    example.
    <td class="zentriert">Mar 4, 1992 (29)</td>
    {'dob': '1992-03-4,', 'age': '29'}

    :param column: bs4 object
    :return: tuple
    """
    if column.text == "- (-)":
        return None, None

    age = None
    dob = None
    if "(" in column.text:
        age = column.text.split(" (")[1].strip(")")
        dob_details = column.text.split(" (")[0]
        if dob_details == "N/A":
            return dob, age

        dob_details = dob_details.split()
        month_str, day, year = dob_details[:3]
        dob = year + "-" + month_number(month_str) + "-" + day.strip(",").zfill(2)

    return dob, age


def nationality(column):
    """
    Get the nationality from the column image

    :param column: bs4 object
    :return: str
    """
    return column.img['title']


def height(column):
    """
    Get the nationality from the column image

    :param column: bs4 object
    :return: str
    """
    return column.text.replace(",", ".")


def foot(column):
    """
    Get a players preferred foot

    :param column: bs4 object
    :return: str
    """
    return column.text if column.text.strip() != "-" else None


def joined(column):
    """
    Get the date a player joined

    :param column: bs4 object
    :return: str
    """
    if column.text == "-":
        return
    joined_str = column.text.split()
    month_str, day, year = joined_str[:3]
    joined_date = year + "-" + month_number(month_str) + "-" + day.strip(",").zfill(2)

    return joined_date


def signed_from(column):
    """
    Get the team a player signed from

    :param column: bs4 object
    :return: str
    """
    return column.img['alt'] if column.img else None


def market_value(column):
    """

    :param column: bs4 object
    :return: str
    """
    return column.text.split(".")[0] if column.text != '-\xa0' else None


def contract_expires(column):
    """
    Get when a players contract expires

    :param column: bs4 object
    :return: str
    """
    if column.text == "-":
        return

    contract_expires_str = column.text.split()
    month_str, day, year = contract_expires_str[:3]
    contract_expires = year + "-" + month_number(month_str) + "-" + day.strip(",").zfill(2)

    return contract_expires


def in_squad(column):
    """
    Get a count of the times a player has appeared in the squad

    :param column: bs4 object
    :return: int
    """
    return column.text if column.text != "-" else 0


def appearances(column):
    """
    Get a count of the times a player has appeared started or come on as sub

    :param column: bs4 object
    :return: int
    """
    return column.text


def goals(column):
    """
    Get a count of the times a player has scored a goal

    :param column: bs4 object
    :return: int
    """
    return '0' if column.text == "-" else column.text


def assists(column):
    """
    Get a count of the times a player has assisted a goal

    :param column: bs4 object
    :return: int
    """
    return '0' if column.text == "-" else column.text


def yellow_cards(column):
    """
    Get a count of the times a player has received a yellow card

    :param column: bs4 object
    :return: int
    """
    return '0' if column.text == "-" else column.text


def second_yellow_cards(column):
    """
    Get a count of the times a player has received a second yellow card

    :param column: bs4 object
    :return: int
    """
    return '0' if column.text == "-" else column.text


def red_cards(column):
    """
    Get a count of the times a player has received a red card

    :param column: bs4 object
    :return: int
    """
    return '0' if column.text == "-" else column.text


def subbed_on(column):
    """
    Get a count of the times a player has been subbed on

    :param column: bs4 object
    :return: int
    """
    return '0' if column.text == "-" else column.text


def subbed_off(column):
    """
    Get a count of the times a player has been subbed off

    :param column: bs4 object
    :return: int
    """
    return '0' if column.text == "-" else column.text


def ppg(column):
    """
    Get a players points per game

    :param column: bs4 object
    :return: int
    """
    return column.text


def minutes_played(column):
    """
    Get a count of the minutes a player has played

    :param column: bs4 object
    :return: int
    """
    return '0' if column.text == "-" else column.text.split("'")[0].replace(".", "")
