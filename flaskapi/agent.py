def name(column):
    """
    Get an agent name

    :param column: bs4 object
    :return: str
    """
    return column.text.strip()


def company(column):
    """
    Get an agent's company name

    :param column: bs4 object
    :return: str
    """
    return column.text.strip()
