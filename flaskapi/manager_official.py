def unique_identifier(column):
    """
    Get a manager or officials id

    :param column: bs4 object
    :return: str
    """
    return column.find("td", {"class": "hauptlink"}).a['id']


def name(column):
    """
    Get a manager or officials name

    :param column: bs4 object
    :return: str
    """
    return column.find("td", {"class": "hauptlink"}).a.text.strip()


def position(column):
    """
    Get a manager or officials position ie Scout, Manager

    :param column: bs4 object
    :return: str
    """
    return column.text.strip()
