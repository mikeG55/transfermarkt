import csv
import config
from bs4 import BeautifulSoup
from api import API
import search
import country
import competition
import team
import player
api = API()


def get_search(search_term):
    """
    Lists relevant players, teams, coaching staff for a search term.

    :param search_term: str
    :return: dict
    """
    output = {}
    response = api.search(search_term)
    if response.status_code != 200:
        return {"message": response.reason,
                "status_code": response.status_code,
                "data": output}

    soup = BeautifulSoup(response.text, 'html.parser')
    content_div = soup.find("div", id="main")
    table_headers = content_div.find_all("div", {"class": "table-header"})
    table_headers = [t.text.split(" - ")[0].strip() for t in table_headers]
    search_tables = content_div.find_all("div", {"class": "responsive-table"})
    search_results = dict(zip(table_headers, search_tables))
    if 'Search results for players' in search_results:
        search_table = search_results['Search results for players'].find('table', {"class": "items"})
        output['Search results for players'] = search.players(search_table)

    if 'Search results: Clubs' in search_results:
        search_table = search_results['Search results: Clubs'].find('table', {"class": "items"})
        output['Search results: Clubs'] = search.teams(search_table)

    if 'Search results: Managers & officials' in search_results:
        search_table = search_results['Search results: Managers & officials'].find('table', {"class": "items"})
        output['Search results: Managers & officials'] = search.managers_and_officials(search_table)

    if 'Search results for agents' in search_results:
        search_table = search_results['Search results for agents'].find('table', {"class": "items"})
        output['Search results for agents'] = search.agents(search_table)

    return {"message": response.reason,
            "status_code": response.status_code,
            "data": output}


def get_continents():
    """

    :return: list
    """
    return {"message": "OK",
            "status_code": 200,
            "data": ['europa', 'asien', 'amerika', 'afrika']}


def get_countries(continent):
    """
    Gets a list of countries for a continent.

    :param continent: str
    :return: list
    """
    output = []
    response = api.get_countries(continent)
    if response.status_code != 200:
        return {"message": response.reason,
                "status_code": response.status_code,
                "data": output}

    soup = BeautifulSoup(response.text, 'html.parser')
    content_div = soup.find("div", id="yw1")
    if content_div is None:
        return {"message": "Not Found",
                "status_code": 404,
                "data": output}

    squad_table = content_div.find("table", {"class": "items"})
    header_row = squad_table.thead.tr
    headers = header_row.find_all('th')
    headers = [h.text for h in headers]
    # ['competition', 'country', 'Clubs', 'player', 'Avg. age', 'Foreigners', 'Game ratio of foreign players',
    # 'Goals per match', 'Forum', 'Average market value', 'Total value']

    content_rows = squad_table.tbody
    content_rows = content_rows.find_all('tr', recursive=False)
    for row in content_rows:
        if not row.has_attr('class'):
            continue

        details = {}
        columns = row.find_all('td', recursive=False)
        row_dict = dict(zip(headers, columns))
        details['id'] = country.unique_identifier(row_dict['country'])
        details['name'] = country.name(row_dict['country'])
        output.append(details)

    return {"message": response.reason,
            "status_code": response.status_code,
            "data": output}


def get_competitions(country_id, season):
    """

    :param country_id: int
    :return:
    """
    output = []
    response = api.get_competitions(country_id, season)
    if response.status_code != 200:
        return {"message": response.reason,
                "status_code": response.status_code,
                "data": output}

    soup = BeautifulSoup(response.text, 'html.parser')
    content_div = soup.find("div", id="yw1")
    if content_div is None or content_div.span.text == "No entries available":
        return {"message": "Not Found",
                "status_code": 404,
                "data": output}

    squad_table = content_div.find("table", {"class": "items"})
    header_row = squad_table.thead.tr
    headers = header_row.find_all('th')
    headers = [h.text for h in headers]
    # ['competition', 'Clubs', 'player', 'Avg. age', 'Foreigners', 'Goals per match', 'Forum', 'Total value']

    content_rows = squad_table.tbody
    content_rows = content_rows.find_all('tr', recursive=False)
    for row in content_rows:
        # only want to process the rows with a class. Most tier labelled rows do not have a class.
        # we do not want these rows at the moment.
        if not row.has_attr('class'):
            continue
        details = {}
        columns = row.find_all('td', recursive=False)
        row_dict = dict(zip(headers, columns))
        details['id'] = competition.unique_identifier(row_dict['competition'])
        details['name'] = competition.name(row_dict['competition'])
        details['slug'] = competition.slug(row_dict['competition'])
        output.append(details)

    return {"message": response.reason,
            "status_code": response.status_code,
            "data": output}


def get_teams(competition_id, season):
    """

    :param competition_slug: str
    :param competition_id: str
    :param season: int
    :return:
    """
    output = []
    response = api.get_teams(competition_id, season)
    if response.status_code != 200:
        return {"message": response.reason,
                "status_code": response.status_code,
                "data": output}

    soup = BeautifulSoup(response.text, 'html.parser')
    content_div = soup.find("div", id="yw1")
    squad_table = content_div.find("table", {"class": "items"})
    header_row = squad_table.thead.tr
    headers = header_row.find_all('th')
    headers = [h.text for h in headers]
    # TODO better check than this?
    # Putting a bad competition id can sometimes bring back the wrong page
    # example this https://www.transfermarkt.com/competition/startseite/wettbewerb/1/saison_id/2020/plus/1
    # with competition_id as 1 brings back page https://www.transfermarkt.com/wettbewerbe/national
    if 'name' not in headers:
        return {"message": 'Not Found',
                "status_code": 404,
                "data": output}

    # ['club', 'name', 'name', 'Squad', 'ø age', 'Foreigners', 'ø age', 'Foreigners', 'Total market value',
    # 'ø market value', 'Total MV', 'ø MV']

    content_rows = squad_table.tbody
    content_rows = content_rows.find_all('tr', recursive=False)
    for row in content_rows:
        details = {}
        columns = row.find_all('td', recursive=False)
        row_dict = dict(zip(headers, columns))
        # TODO use main name not shortened name
        details['id'] = team.unique_identifier(row_dict['name'])
        details['name'] = team.name(row_dict['name'])
        details['slug'] = team.slug(row_dict['name'])
        output.append(details)

    return {"message": response.reason,
            "status_code": response.status_code,
            "data": output}


def get_squad(team_id, season):
    """

    :param team_slug: str
    :param team_id: int
    :param season: int
    :return:
    """
    output = []
    response = api.get_squad(team_id, season)
    if response.status_code != 200:
        return {"message": response.reason,
                "status_code": response.status_code,
                "data": output}

    soup = BeautifulSoup(response.text, 'html.parser')
    content_div = soup.find("div", id="yw1")
    squad_table = content_div.find("table", {"class": "items"})
    header_row = squad_table.thead.tr
    headers = header_row.find_all('th')
    headers = [h.text for h in headers]
    # ['#', 'player', 'Date of birth / Age', 'Nat.', 'Height', 'Foot', 'Joined', 'Signed from', 'Contract expires',
    # 'Market value']

    content_rows = squad_table.tbody
    content_rows = content_rows.find_all('tr', recursive=False)
    for row in content_rows:
        details = {}
        columns = row.find_all('td', recursive=False)
        row_dict = dict(zip(headers, columns))
        details['name'] = player.name(row_dict['player'])
        details['dob'], details['age'] = player.dob(row_dict['Date of birth / Age'])
        # TODO deal with dual nationality.
        details['nationality'] = player.nationality(row_dict['Nat.'])
        # TODO if historic season parser current club. Also player age relevant to season.
        details['height'] = player.height(row_dict['Height'])
        details['foot'] = player.foot(row_dict['Foot'])
        details['joined'] = player.joined(row_dict['Joined'])
        details['signed_from'] = player.signed_from(row_dict['Signed from'])
        details['contract_expires'] = player.contract_expires(row_dict['Contract expires'])
        details['market_value'] = player.market_value(row_dict['Market value'])
        output.append(details)

    return {"message": response.reason,
            "status_code": response.status_code,
            "data": output}


def get_squad_stats(team_id, season):
    """

    :param team_id:
    :param season:
    :return:
    """
    output = []
    response = api.get_squad_stats(team_id, season)
    if response.status_code != 200:
        return {"message": response.reason,
                "status_code": response.status_code,
                "data": output}

    soup = BeautifulSoup(response.text, 'html.parser')
    content_div = soup.find("div", id="yw1")
    if content_div is None or content_div.span.text == "No information":
        return {"message": "Not Found",
                "status_code": 404,
                "data": output}

    squad_table = content_div.find("table", {"class": "items"})
    header_row = squad_table.thead.tr
    headers = header_row.find_all('th')
    headers = [h.span['title'] if h.span else h.text for h in headers]
    # ['#', 'player', 'Age', 'Nat.', 'In squad', 'Appearances', 'Tore', 'Assists', 'Yellow cards',
    # 'Second yellow cards', 'Red cards', 'Substitutions on', 'Substitutions off', 'PPG', 'Minutes played']

    content_rows = squad_table.tbody
    content_rows = content_rows.find_all('tr', recursive=False)
    for row in content_rows:
        details = {}
        columns = row.find_all('td', recursive=False)
        row_dict = dict(zip(headers, columns))
        details['name'] = player.name(row_dict['player'])
        details['in_squad'] = player.in_squad(row_dict['In squad'])
        details['appearances'] = player.appearances(row_dict['Appearances'])
        details['goals'] = player.goals(row_dict['Tore'])
        details['assists'] = player.assists(row_dict['Assists'])
        details['yellow_cards'] = player.yellow_cards(row_dict['Yellow cards'])
        details['second_yellow_cards'] = player.second_yellow_cards(row_dict['Second yellow cards'])
        details['red_cards'] = player.red_cards(row_dict['Red cards'])
        details['subbed_on'] = player.subbed_on(row_dict['Substitutions on'])
        details['subbed_off'] = player.subbed_off(row_dict['Substitutions off'])
        details['ppg'] = player.ppg(row_dict['PPG'])
        details['minutes_played'] = player.minutes_played(row_dict['Minutes played'])
        output.append(details)

    return {"message": response.reason,
            "status_code": response.status_code,
            "data": output}


def squad_details_to_csv(team_id, season):
    """

    :return:
    """
    squad_details = get_squad(team_id, season)
    keys = squad_details[0].keys()
    outfile = f"{config['app']['root_path']}/{config['app']['project_name']}/data/csv/{team}.csv"
    with open(outfile, 'w') as output_file:
        dict_writer = csv.DictWriter(output_file, fieldnames=keys, delimiter=',')
        dict_writer.writeheader()
        dict_writer.writerows(squad_details)

    return outfile
