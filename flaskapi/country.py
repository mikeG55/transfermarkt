def name(column):
    """
    Get country name

    :param column: bs4 object
    :return: str
    """
    return column.img['title']


def unique_identifier(column):
    """
    Get country id

    :param column: bs4 object
    :return: str
    """
    return column.img['src'].split("/")[-1].split(".png")[0]
