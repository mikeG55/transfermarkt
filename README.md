# transfermarkt.

Unofficial transfermarkt api

## Build.
`docker-compose build`

`docker-compose up`


## API Docs.
### Get continents

**Definition**

- `GET /continents`

**Response**
- `200 OK` on Success

```json
    {
        "message": "OK",
        "data": ["europa", "asien", "amerika", "afrika"] 
    }

```

### Get countries

**Definition**

- `GET /countries/<continent>`

**Response**
- `200 OK` on Success

```json
    {
        "message": "OK",
        "data": [{"id": "189", "name": "England"}, {"id": "75", "name": "Italy"}, {"id": "157", "name": "Spain"}, 
          {"id": "40", "name": "Germany"}, {"id": "50", "name": "France"}] 
    }

```

### Get competitions

**Definition**

- `GET /competitions/<country_id>/<season>`

**Response**
- `200 OK` on Success

```json
    {
        "message": "OK",
        "data": [{"id": "GB1", "name": "Premier League", "slug": "premier-league"}, 
          {"id": "GB2", "name": "Championship", "slug": "championship"}, 
          {"id": "GB3", "name": "League One", "slug": "league-one"}, 
          {"id": "GB4", "name": "League Two", "slug": "league-two"}] 
    }

```

### Get teams

**Definition**

- `GET /teams/<competition_id>/<season>`

**Response**
- `200 OK` on Success

```json
    {
        "message": "OK",
        "data": [{"id": "281", "name": "Man City", "slug": "manchester-city"}, 
          {"id": "31", "name": "Liverpool", "slug": "fc-liverpool"}, 
          {"id": "631", "name": "Chelsea", "slug": "fc-chelsea"}, 
          {"id": "985", "name": "Man Utd", "slug": "manchester-united"}] 
    }

```

### Get squad

**Definition**

- `GET /squad/<team_id>/<season>`

**Responses**
- `200 OK` on Success

```json
    {
        "message": "OK",
        "data": [] 
    }

```

### Get squad stats

**Definition**

- `GET /squad_stats/<team_id>/<season>`

**Response**
- `200 OK` on Success

```json
    {
        "message": "OK",
        "data": [] 
    }

```